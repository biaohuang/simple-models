function [A, B, C, Bd, Q, R ]=MatrixContructionFunctionNew(a,b,c,bd, la, eMAVE, duMAVE) 

% This function creates MPC matrices based on
% Van Overloop, P. J. (2006). Model predictive control on open water systems. IOS Press.
% PhD Thesis, Delft University of Technology

% The matrices are squipping the first row (current situation) as opposed
% to the thesis

%% Reading the sizes
[out_dim, ~]=size(c);
[~, dist_dim]=size(bd);
[n, m]=size(b);


%% Creating matrix A

A=a;
for i=2:la
    Z=zeros(n*(i-1),n);
    A=[A ; a^(i)];
end

%A=[eye(n);A]; %This would be for PJ formulation
A=[A];

%% Construction of B

sf=[b zeros(n, (la-1)*m)];
for k=0:la-2
   s=[a^(k+1)*b];
   for i=1:la-1
      p=k-i+1;

      if p<0
          p=0;
          z=zeros(n,m);
      else
                z=a^p*b;
      end

      s=[s z];
   end
    sf=[sf;s];
    
end
%B=[zeros(n,la*m);sf];
B=[sf];

%% Construction of Bd

clear s
clear sf

sf=[bd zeros(n, (la-1)*dist_dim)];
for k=0:la-2
   s=[a^(k+1)*bd];
   for i=1:la-1
      p=k-i+1;
      if p<0
          p=0;
          z=zeros(n,m);
      else
          z=a^p*bd;
      end
      s=[s z];
   end
    sf=[sf;s];
end
%Bd=[zeros(n,la*dist_dim);sf];
Bd = sf;
%% Construction of C and D
c1=c;
for k=1:la-1
    z1=zeros(out_dim*k,n);
    z2=zeros(out_dim,k*n);
    c1=[c1, z1 ;z2 c];
end
C=c1;

%% Construction of the weighing matrices
if duMAVE == 0
    r = zeros(m,m);
else
    r=eye(m) * 1/(duMAVE^2);
end

if eMAVE == 0
    q_out = zeros(out_dim, out_dim);
else
    q_out = eye(out_dim) * 1 / (eMAVE^2) ;
end
q = c'*q_out*c;

R=[r];
for k=1:la-1
  z=zeros(m*k,m);
  R=[R z; z' r];
end

Q=[q];
for k=1:la-1
  z=zeros((n)*k,(n));
  Q=[Q z; z' q];
end

