%Example
clear; close all; clc
addpath('../Final/LinearModels')
addpath('../Final')

q=1;                      %initial inflow
n=0.02;                     %Manning
B=3;                        %Bottom width
m=0;                        %Side slope
Sb=0.0003;                  %Bottom slope
Y0=3;                      %can't be lower as 1.4 // Down stream water level [m]
h_real_initial=Y0;          %small error (0.999999999) in the beginning to see the effect of the controller later on. 
h_setpoint=Y0;              %setpoint of waterlevel [m]
L=7000;                     %Length [m]
kh=0;                       %Nothing
Bw=B;
q_downstream=2.5;              %downstream discharge
q_steady=q;
q_nominal=q;
NodeNumGiven=30;

[ae tau]=IdFun(q,n,B,m,Sb,Y0,L,kh);

%% tuning variables
sampling_time=900;          %what determines the max value??
dt = sampling_time/10;      %Should be an integer
simulation_time=tau*30;
% simulation_time=57500;


%Uncomment this if you would like to see the profile
% [ WaterProfile, distance] = SteadyState(L, m,Bw,n,Sb,q,Y0,100, 0);
% plot(WaterProfile)
%% Create a system by hand
         

%Necessary discharge states, from delay
%The k is equal to q(k-delay, rounded up)
%The k+1 is the q(k-dealy rounded down), 
%so the number of the states is delay rounded down +1

q_lines = floor (tau / sampling_time);
system_size = q_lines +1;
NodeNum=floor(NodeNumGiven/2)*2-1;
system_size=NodeNum-2; 
% system_size = 27;

% a (1, end) = sampling_time / ae;
% for k = 3:q_lines +1
%     a(k,k-1)=1;
% end
% 
% b = zeros (system_size, 2);
% b(2,1)=1;
% b(1,2)=-sampling_time / ae;
% 
c = zeros (1, system_size);
c (end) = 1;

%% Simulation
la = 27;

simulation_time_vector= 0:sampling_time:simulation_time;
t= 0:sampling_time/3600:simulation_time/3600;     %for adding time-scale to the plots
tt= 0:sampling_time/3600:(simulation_time-1*sampling_time)/3600;    %for adding time-scale to the plots
u = zeros(2, length(simulation_time_vector)+la);
%u(2,:)= ones(1,length(simulation_time_vector));
u(1,1) = 0;
u(2,14) = q_downstream-q_steady;
u(2,15) = q_downstream-q_steady;
u(2,16) = q_downstream-q_steady;

x0 = zeros(system_size, 1);
x0(1,1)=h_real_initial-h_setpoint;

%% Creating the big matrices
% %la = length(u);
% [A, B, C, Bd, Q, R ]= MatrixContructionFunctionNew(a,b(:,1),c,b(:,2), la, 0.002, 0.03);
% %[A, B, C, Bd, Q, R ]= MatrixContructionFunctionNew(a,b(:,1),c,b(:,2), la, 0.2, 0.1);

ModelType=2;
[x, A, b, D,Hinit]=LinearInertialImp(m,B,Y0,n,q_nominal,Sb,L,sampling_time,NodeNumGiven,ModelType);
a=A;
%b=Bd;
[A, B, C, Bd, Q, R ]= MatrixContructionFunctionNew(a,b(:,1),c,b(:,2), la, 0.01, 0.3);



%% Now creating a control

x = x0;
y2_real(1)=h_real_initial;
xmodel=x0;

UseInitialization =2;       %??
HinitGiven  = 0;            %??
y4(1)=h_real_initial;

D_time=zeros (la, 1);
%for k = 1 : floor(simulation_time/sampling_time)
for k = 1 : 22
    
         D_time(1:la) = u(2,k:k+la-1);

%     xtest = a * x + b * [u(1,1);D_time(1)];      %updating discharge in x
%     x = xtest;
      xmodel=a*xmodel+b*[u(1,1);D_time(1)];
      x=xmodel;
      x(27)=y4(k)-h_setpoint;        %updating the water level difference every time step
      %x(1,1)= xmodel(end);
%     %Updating disturbance

    H=2*(B'*Q*B+R);
    f=2*x'*(A'*Q*B)+2*D_time'*(Bd'*Q*B);        

    [xres,fval,exitflag]=quadprog(H,f);
    u(1,1:la)=xres';
    
    %Creates Q inputs (long vectors) for the simulation
    qu_used=ones(floor(sampling_time/dt)+4,1)*(u(1,1)+q_steady);       %real controlled input   
    qd_used=ones(floor(sampling_time/dt)+4,1)*(u(2,k)+q_steady);       %real disturbance
    if k==1
        [ Time,Hm2 , Hm,HinitH] =FiniteVoumeModel11bck(L, m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300, 0, 0);
    else
        [ Time,Hm2 , Hm,HinitH] =FiniteVoumeModel11bck(L, m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300, 1, Hm2(end,:) );
    end
    
    %Saving variables
    y4(k+1)=Hm2(end,end);
    u_m(k)=u(2,k);
    u_m2(k)=u(1,1);
    
    Error(k+1)=abs(y4(k+1)-Y0);         %for analyses
end
y2_real(1)=h_real_initial;

%% analyses
y_ana=zeros(1,floor(simulation_time/sampling_time));
y_ana(1,:)=Y0; %*(1-0.005*q_downstream);
Error_steady=abs(y4(end)-Y0);            %for analyses
Error_max=max(Error);                    %for analyses
Error_mean=mean(Error);                  %for analyses

fprintf('Steady state error = %f m\n', Error_steady);
fprintf('Maximum error      = %f m\n', Error_max);
fprintf('Mean error         = %f m\n', Error_mean);

%%
figure
subplot(2,1,1)
plot(y4,'--*');grid
hold on
plot(tt,y_ana)
title('Water level (downstream)')
xlabel('time (h)')
ylabel('water level (m)')

subplot(2,1,2)
plot(u_m, '-*')
hold on
plot(u_m2, '-*r');grid
% legend('Downstream (given) Q', 'Upstream (Controlled) Q','location','BestOutside')
title('Discharges')
xlabel('time (h)')
ylabel('discharge (m3/s)')