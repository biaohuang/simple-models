%Model comparison
%Klaudia Horv�th, Delft, 2016
%Modified: 2017.04.07.

%this file compares step responses
%It does not load anything
%It uses functions like:
% -
% -
% -

%Later to do:
%Implement in Hayami if something comes from downstream
%Implemetn the dossteream normally for ID and for the ohter stuff

%This is the same like 4, but without Sobek
clear 
close all
addpath('..\Final')
addpath('../Final/LinearModels')
addpath('../DistributedModels')

%The physical data of the reach
ToSave=1;
CaseNumber=1;
StepNumber=2;
ModelType=2; % ModelType=2; %LinearizedInertial % ModelType=1; %Lineareized SV % ModelType=3; %Linear model 

dt=10; %For case 1 use 0.5
EndTime=5*3600;
NodeNumGiven=30;
%NodeNumGiven=50;

%for case 5 node numer is
%NodeNumGiven=18;

%Case 1
% dt=0.25; %For case 1 use 0.5
% EndTime=2*3600;
% NodeNumGiven=8;


%% Data channels (zelfde als bij het andere script)
%load ChannelData

lm=7000;
zm=2;
bm=3;
nm=0.02;
sm=1.5e-4;
wm=1.5;
hminm=0.5;
hmaxm=1.5;
qqm=2.5;
qminm=2;
qmaxm=3;


% lm=7000;
% zm=0;
% bm=3;
% nm=0.02;
% sm=3e-4;
% wm=1;
% hminm=0.5;
% hmaxm=1.6;
% qqm=1.5;
% qminm=2;
% qmaxm=3;

%Nonlinearity parameter
%how widht change of water depth changes the bw area
%How the water dpeth changes with the voulume --> Backwater area is small,
%even when we have backwater, so L*B is small compared to the volume
%Or the backwater area changes a lot, that occurs when 
%Delta q / (LB) here 0.5/
%Say if a normal discharge thing can cause more than 80 cm, then wrong.
%dq/(LB)<0.8

% pick initial values for 1 case
n=nm(CaseNumber);
Sb=sm(CaseNumber);
L=lm(CaseNumber);
B=bm(CaseNumber);
m=zm(CaseNumber);
q=qqm(CaseNumber); %This is the discharge around which we linearize
Y0=wm(CaseNumber); %This is the h around which we liearize
qmax=qmaxm(CaseNumber);

DischargeStepMatrix=[q qqm qmax];
%DischargeStep=DischargeStepMatrix(StepNumber);
DischargeStep=qmax-qqm;
NominalDischarge=qqm;
kh=0;
StepFinish=floor(3600/dt);

Distance=L/(NodeNumGiven-2);
NumberHPoints=(NodeNumGiven-2)/2;
XMatrix=[];   
sxx=(Sb-sf0(Y0,q,n,B,m))/(1-(froude(Y0,q,n,B,m))^2);

%Initialize variables
x2M=zeros(NumberHPoints,1);
y2M=zeros(NumberHPoints,1);

 p21_infM=zeros(NumberHPoints,1);
%  tdM(k)=zeros(NumberHPoints,1);
%  tauIDupM(k)=zeros(NumberHPoints,1);
tdMD=zeros(NumberHPoints,1);
tauIDupMD=zeros(NumberHPoints,1);
IDDownAs=zeros(NumberHPoints,1);
IDUpAs=zeros(NumberHPoints,1);
HayR=zeros(NumberHPoints,1);
HayParameters=zeros(NumberHPoints,5);
aa=zeros(NumberHPoints,1);
bb=zeros(NumberHPoints,1);
cc=zeros(NumberHPoints,1);
dd=zeros(NumberHPoints,1);
ee=zeros(NumberHPoints,1);
tauhay=zeros(NumberHPoints,1);
p12_infM=zeros(NumberHPoints,1);
tuMD=zeros(NumberHPoints,1);
tauIDdownMD=zeros(NumberHPoints,1);
auM=zeros(NumberHPoints,1);


%First model
[~, ~, ~, ~, au, ad, ~, ~, yn,xmid]=IdzFun(q,n,B,m,Sb,Y0,L);
xsep=2*xmid-L;
[AdownID, tau]=IdFunBw(q,n,B,m,Sb,Y0,L,kh); 
AupID=1e9; %Upstream nothing happens




%% This loop caclulates the parameters for all water level points in a reach
for k=1:NumberHPoints
    x2=Distance+Distance*2*(k-1); %x2 is the horizontal distance from upstream of the chosen point
    y2=Y0-(L-x2)*sxx; %y2 is the water level belonging to x2
    if y2<yn
        y2=yn;
    end
    %Stores in matrix the longitudinal distance and the corresponding water
    %level
    x2M(k)=x2;
    y2M(k)=y2; %good
    %This is the transfer function from upstream to a certain point
    %That is why we are taking the corresponding water depth (that we
    %calculate) and the length that is from upstream to that point (x2)
    %For p21 inf and taud Up, the upstream part
    [~, ~, p21_inf , ~, ~, ~, ~, td , ~]=IdzFun(q,n,B,m,Sb,y2,x2);
    [~, tau]=IdFunBw(q,n,B,m,Sb,Y0,x2,kh); 
    [aa_ bb_ cc_ dd_ ee_ r cl yn cnoni dnoni enoni]=FunHayami(q,n,B,m,Sb,y2,x2,kh,dt);
    tauhay(k)=r;
    %HayParameters(k,:)=[aa bb cc dd ee];
    if x2<xsep
           IDUpAs(k)=AupID;
    else
           IDUpAs(k)=AdownID;
    end
    aa(k)=aa_;
    bb(k)=bb_;
    cc(k)=cnoni/IDUpAs(k);
    dd(k)=dnoni/IDUpAs(k);
    ee(k)=enoni/IDUpAs(k);
    p21_infM(k)=p21_inf; %transfer function between ups q and down h
    tdMD(k)=floor(td/dt); %the discretized parameters
    tauIDupMD(k)=floor(tau/dt); %the discretized parameters
    %----------------------------------------------------------------------
    %These are the effects from downstream to all water levels
    [~, p12_inf_ , ~, ~, ~, ~, tu_ , ~, ~]=IdzFun(q,n,B,m,Sb,Y0,L-x2);  %Think about this claim for later for the distributed
    [~, tau]=IdFunBw(q,n,B,m,Sb,Y0,L-x2,kh);
    IDDownAs(k)=AdownID; %This should be an if statement such as above
    p12_infM(k)=p12_inf_;
    tuMD(k)=floor(tu_/dt);
    tauIDdownMD(k)=floor(tau/dt);
     %The backwater area is calculated proportinally from the originals
     %Write these findings down
     %So... if x2 is up, use au, if lower, use ad
%    auM(k)=x2/L*au+(L-x2)/L*ad;
     if x2<xsep
         auM(k)=au;
     else
         auM(k)=ad; 
     end
end

%%

[p11_inf, p12_inf, p21_inf, p22_inf, au, ad, tu, td, yn, yu]=IdzFun(q,n,B,m,Sb,Y0,L);
%Calculating the parameters of the ID model

[a, tau]=IdFunBw(q,n,B,m,Sb,Y0,L,kh);

%The linearized inertial model
[x, A, Bd, D,Hinit]=LinearInertial(m,B,Y0,n,q,Sb,L,dt,NodeNumGiven,ModelType);

%Hayami
%[aa, bb cc dd ee tauhay, cl, yn]=FunHayami(q,n,B,m,Sb,Y0,L,kh,dt);


SimLength=floor(EndTime/dt)+1;
%Note: you calculate the first water level not in the very end of the canal
%in order to follow the other type of discretization (for the SV and Lin
%In)
InitializationTime=max([max(tauIDdownMD) max(tuMD) max(tauIDupMD) max(tdMD) max(tauhay)])+100;

%InitializationTime=floor(max([tau tu td])/dt)+1;
Factor=3600;

%ID model
taud=floor(td/dt);
tauidzup=floor(tu/dt);
taudid=floor(tau/dt);

StartTime=InitializationTime;
hidz(1:StartTime)=0;
hid(1:StartTime)=0;
%hhay(1:StartTime)=0;


hidzup(1:StartTime)=0;
hidup(1:StartTime)=0;

hidzmid(1:StartTime)=0;
hidmid(1:StartTime)=0;

q=ones(SimLength+InitializationTime+2,1)*0;
q(1:InitializationTime+StepFinish,1)=DischargeStep;
q(1:InitializationTime)=0;

qd=ones(SimLength+InitializationTime+2,1)*0;
qd(1:InitializationTime)=0;

% SV
[ Time,Hm2 , Hm,~] = InitialiseFiniteVolume(L, zm,B,n,Sb,EndTime,dt,q+NominalDischarge,qd+NominalDischarge,wm ,NodeNumGiven);
hmid=ones(SimLength+InitializationTime+2,NumberHPoints)*0;
hmidz=ones(SimLength+InitializationTime+2,NumberHPoints)*0;
hhay=ones(SimLength+InitializationTime+2,NumberHPoints)*0;
HinitH=Hm2(1,:);
%The approximation of the downstream water level
for k=StartTime:SimLength+InitializationTime+1
    for Nodes=1:NumberHPoints
      hmidz(k+1,Nodes)=hmidz(k,Nodes)+dt*q(k-tdMD(Nodes))/auM(Nodes)+p21_infM(Nodes)*(q(k+1-tdMD(Nodes))-q(k-tdMD(Nodes)))...
                       -dt*qd(k-tuMD(Nodes))/auM(Nodes)-p12_infM(Nodes)*(qd(k+1-tuMD(Nodes))-qd(k-tuMD(Nodes))); %This should be adm, it it is calculated at all...
%      hmid(k+1,Nodes)=  hmid(k,Nodes)+dt*q(k-tauIDupMD(Nodes))/a-dt*qd(k-tauIDdownMD(Nodes))/a;
       hmid(k+1,Nodes)=  hmid(k,Nodes)+dt*q(k-tauIDupMD(Nodes))/IDUpAs(Nodes)-dt*qd(k-tauIDdownMD(Nodes))/IDDownAs(Nodes);
       hhay(k+1,Nodes)=(aa(Nodes)+1)*hhay(k,Nodes)+(-bb(Nodes)-aa(Nodes))*hhay(k-1,Nodes)+...
                        bb(Nodes)*hhay(k-2,Nodes)+cc(Nodes)*q(k-tauhay(Nodes))+dd(Nodes)*q(k-tauhay(Nodes)-1)+ee(Nodes)*q(k-tauhay(Nodes)-2);
    end
end

%hmidzf=hmidz(1+InitializationTime:SimLength+InitializationTime,:);
%hmidf=hmid(1+InitializationTime:SimLength+InitializationTime,:);

hmidzf=hmidz(1:SimLength,:); %changed 0404
hmidf=hmid(1:SimLength,:);
hmhay=hhay(1:SimLength,:);

%Simulation with the linear inertial model
for k=1:SimLength 
    xold=x;
    x=A*x+Bd*D;
    qb=q(k);
    hb=0;
%     if  k>0 && k<StepFinish
%         qb=0; %the extra q step
%         qb=DischargeStep;
%         hb=0;
%     else
%         qb=0;
%         hb=0;
%     end
    D=[qb;hb];
    hm(k,:)=xold(1:2:end)';
    qm(k,:)=xold(2:2:end)';
    hine(k)=hm(k,end);
end

%%
%Plot parameters
MyFontSize=12;
MyFont='Arial';
MyGrey=[1 1 1]*0.6;

f=figure(1);
t=0:dt:(SimLength-1)*dt;
InitialProfile=HinitH;
plot(t/Factor,hmidzf(:,end)+InitialProfile(end),'Color',MyGrey,'LineWidth',2)
hold on
plot(t/Factor,hmidf(:,end)+InitialProfile(end),'--','Color',MyGrey)
plot(t/Factor,hm(:,end)+InitialProfile(end),'-','Color','red')
plot(t/Factor,Hm2(1:end,end),'-','Color','blue')
plot(t/Factor,hmhay(:,end)+InitialProfile(end),'-','Color','green')


%xlim([0 40])
title(['Comparison of step responses, 200% upstream Q, Case ' num2str(CaseNumber)],'Fontsize',MyFontSize,'FontName',MyFont)
legend('IDZ','ID','Inertial','SV','Hayami','Location','SouthEast')
xlabel('Time (h)','Fontsize',MyFontSize,'FontName',MyFont)
ylabel('Downstream water depth (m)','Fontsize',MyFontSize,'FontName',MyFont)
set(gca,'fontsize', MyFontSize,'FontName',MyFont)
FigName1=['WaterLevDown' num2str(CaseNumber) 'Step' num2str(StepNumber)];
if ToSave==1
  print(f,FigName1,'-djpeg','-r0')
  saveas(f,FigName1,'epsc')
end


Location=1;
f=figure(2);
t=0:dt:(SimLength-1)*dt;
InitialProfile=HinitH;
plot(t/Factor,hmidzf(:,Location)+InitialProfile(Location),'Color',MyGrey,'LineWidth',2)
hold on
plot(t/Factor,hmidf(:,Location)+InitialProfile(Location),'--','Color',MyGrey)
plot(t/Factor,hm(:,Location)+InitialProfile(Location),'-','Color','red')
plot(t/Factor,Hm2(1:end,Location),'-','Color','blue')
plot(t/Factor,hmhay(:,Location)+InitialProfile(Location),'-','Color','green')


%xlim([0 40])
title(['Comparison of step responses, 200% upstream Q, Case ' num2str(CaseNumber)],'Fontsize',MyFontSize,'FontName',MyFont)
legend('IDZ','ID','Inertial','SV','Hayami','Location','SouthEast')
xlabel('Time (h)','Fontsize',MyFontSize,'FontName',MyFont)
ylabel('Downstream water depth (m)','Fontsize',MyFontSize,'FontName',MyFont)