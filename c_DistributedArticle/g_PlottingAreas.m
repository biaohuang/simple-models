%testintergator
clear
close all
addpath('../Data/Bode')
ar=100;

%Plot with two integrators
f3=figure(1)

sys=tf([1],[ar 0]);
bode(sys)
hold on
sys2=tf([1],[ar*100 0]);
bode(sys2)
legend('Area: 100','Area: 10000')
%print(f3,'integrator_areas','-dpng','-r300')

% Bode plot by hand
figure;
w=logspace(0,1,10);
[mag_, phase, freqi]=bode(sys,w);
mag=squeeze(mag_(1,1,:));
semilogx(freqi,20*log10(mag))



%Checking how the area is changing in a shorter canal
f1=figure;
load bode_small_canal_nondist
area11=1./(10.^(p11_mag./20).*freq);
area21=1./(10.^(p21_mag./20).*freq);
area12=1./(10.^(p12_mag./20).*freq);
area22=1./(10.^(p22_mag./20).*freq);



plot(area11(1:1200))
hold on
plot(area12(1:1200))
plot(area21(1:1200))
plot(area22(1:1200))
legend('p11','p12','p21','p22')
title('Backwater area canal 1')
%print(f1,'canal_1_bw_area','-dpng','-r300')

%Checking how the area is changing in a longer canal
f2=figure;
load bode_big_canal_nondist
area11=1./(10.^(p11_mag./20).*freq);
area21=1./(10.^(p21_mag./20).*freq);
area12=1./(10.^(p12_mag./20).*freq);
area22=1./(10.^(p22_mag./20).*freq);


plot(area11)
hold on
plot(area12)
plot(area21)
plot(area22)
legend('p11','p12','p21','p22')
title('Backwater area canal 2')
%print(f1,'canal_2_bw_area','-dpng','-r300')
