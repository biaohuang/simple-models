%Model comparison
%Klaudia Horv�th, Delft, 2016
%Modified: 2017.04.07.

%Later to do:
%Implement in Hayami if something comes from downstream
%Implemetn the dossteream normally for ID and for the ohter stuff

%Id
%IDZ
%hayami
%SV

%IR
%In


%This is the same like 4, but without Sobek
clear 
close all
addpath('..\Final')
addpath('../Final/LinearModels')

%The physical data of the reach
ToSave=1;
CaseNumber=1;
StepNumber=2;
ModelType=2; % ModelType=2; %LinearizedInertial % ModelType=1; %Lineareized SV % ModelType=3; %Linear model 

dt=10; %For case 1 use 0.5
EndTime=5*3600;
NodeNumGiven=30;
%NodeNumGiven=50;

%for case 5 node numer is
%NodeNumGiven=18;

%Case 1
% dt=0.25; %For case 1 use 0.5
% EndTime=2*3600;
% NodeNumGiven=8;


% Data channels (zelfde als bij het andere script)
%load ChannelData

lm=8000; %It works well for 700
zm=2;
bm=3;
nm=0.02;
sm=1.5e-4;
wm=1.5;
hminm=0.5;
hmaxm=1.5;
qqm=2.5;
qminm=2;
qmaxm=3;


%Litrico canal1
lm=3000;
zm=1.5;
bm=7;
sm=1e-4;
nm=0.02;
wm=1.12;
qqm=14;


%My canal1
lm=3000;
zm=0;
bm=3;
sm=0.0015;
nm=0.03;
wm=3;
qqm=1.5;

% %Litrico canal2
% lm=6000;
% zm=1.5;
% bm=8;
% sm=8e-4;
% nm=0.02;
% wm=2.92;
% qqm=80;

% lm=7000;
% zm=0;
% bm=3;
% nm=0.02;
% sm=3e-4;
% wm=1;
% hminm=0.5;
% hmaxm=1.6;
% qqm=1.5;
% qminm=2;
% qmaxm=3;

%Nonlinearity parameter
%how widht change of water depth changes the bw area
%How the water dpeth changes with the voulume --> Backwater area is small,
%even when we have backwater, so L*B is small compared to the volume
%Or the backwater area changes a lot, that occurs when 
%Delta q / (LB) here 0.5/
%Say if a normal discharge thing can cause more than 80 cm, then wrong.
%dq/(LB)<0.8

% pick initial values for 1 case
n=nm(CaseNumber);
Sb=sm(CaseNumber);
L=lm(CaseNumber);
B=bm(CaseNumber);
m=zm(CaseNumber);
q=qqm(CaseNumber); %This is the discharge around which we linearize
Y0=wm(CaseNumber); %This is the h around which we liearize
qmax=qmaxm(CaseNumber);

DischargeStepMatrix=[q qqm qmax];
%DischargeStep=DischargeStepMatrix(StepNumber);
DischargeStep=qmax-qqm;
NominalDischarge=qqm;
kh=0;
StepFinish=floor(3600/dt);

Distance=L/(NodeNumGiven-2);
NumberHPoints=(NodeNumGiven-2)/2;
XMatrix=[];   
sxx=(Sb-sf0(Y0,q,n,B,m))/(1-(froude(Y0,q,n,B,m))^2);

%Initialize variables
x2M=zeros(NumberHPoints,1);
y2M=zeros(NumberHPoints,1);

 p21_infM=zeros(NumberHPoints,1);
%  tdM(k)=zeros(NumberHPoints,1);
%  tauIDupM(k)=zeros(NumberHPoints,1);
tdMD=zeros(NumberHPoints,1);
tauIDupMD=zeros(NumberHPoints,1);
IDDownAs=zeros(NumberHPoints,1);
IDUpAs=zeros(NumberHPoints,1);
HayR=zeros(NumberHPoints,1);
HayParameters=zeros(NumberHPoints,5);
aa=zeros(NumberHPoints,1);
bb=zeros(NumberHPoints,1);
cc=zeros(NumberHPoints,1);
dd=zeros(NumberHPoints,1);
ee=zeros(NumberHPoints,1);
tauhay=zeros(NumberHPoints,1);
p12_infM=zeros(NumberHPoints,1);
tuMD=zeros(NumberHPoints,1);
tauIDdownMD=zeros(NumberHPoints,1);
auM=zeros(NumberHPoints,1);


%Backwater area options

%1. No splitting

%2. Splitting Litrico

%3. Splitting at normal depth

g=9.81;

kh=0;       %initial height = 0
sb=Sb;      %bottom slope
Man=n;      %Manning coefficient??
b0=B;       %Bottom width
h=Y0;       %initial height??
yx=h;       %downstream height
g=9.81;

%Calculationg the divison points in case of other canals-------------------

% Calculating the normal depth

    %Normal depth
    
    y1=0;                                   %We suppose it is between 0 and 5 times the original depth
    y2=yx*5;        
    for k=1:25          
      y=(y1+y2)/2;                          %first guess (middle of the range)
      a=b0*y+m*y^2;                         %using average height to calculate area     
      p=b0+2*y*(1+m^2)^0.5;                 %wet perimeter calculated trapezoidal cross section
      r=a/p;                                %hydrolic radius
      dif=(q^2*n^2)/(a^2*r^(4/3))-sb;       %friction slope - bottom slope
      if dif<0                              % if bottom slope > friction slope, we are under normal depth
          y2=(y1+y2)/2;
      else                                  % if bottom slope < friction slope, we are over normal depth, lower range is moved to half
          y1=(y1+y2)/2;
      end
    end
    yn=y;                                   %Normal depth


     %Angle between the bottom line and the water surface
     sxx=(sb-sf0(yx,q,n,b0,m))/(1-(froude(yx,q,n,b0,m))^2); 
     %yu=yx-(L)*sxx; %good

    if abs(sxx)<1e-8  %If the angle is zero, normal depth
        x1_calc=L;    %The end of the normal depth zone is in the end of the channel (no bw)
    else
        x1_calc=max(0,L-(yx-yn)/sxx); %Calculate the point where normal depth is reached
    end
    x1=x1_calc;
    %Calculating the corresponding water level
    if x1==0  
        y1=yx-sxx*L;
    else
        y1=yn;
    end



Lbw=L-x1;  %Length of the backwater part
Lunif=x1;  %Length of the uniform part



% Part 3 ----Time delay and backwater area---------------------------------
 y2=yx; %This is the (downstream) water depth we use for calculation.  (should it be the middle...?)
 a=b0*y2+m*y2^2;  %cross sectional area
 t=b0+2*y2*m;     %top width
 c=(g*a/t)^0.5;   %celerity
 v=q/a;           %velocity
 tau=Lunif/(c+v)/2+Lunif/(c-v)/2;  %time delay, uniform part only
 ae=Lbw*t;                         %backwater surface (bw area only)
 a1=ae;

%[AdownID, tau]=IdFunBw(q,n,B,m,Sb,Y0,L,kh); 

 

[ Timex,Hm2x , Hmx,HinitH1] = FiniteVoumeModel11bck(L, m,B,n,Sb,1,dt,ones(10,1)*q,ones(10,1)*q,Y0,165, 0, 0 );
                              
 HinitH=SteadyState(L, m,B,n,Sb,q,h,165 ) ;          
            %Normal depth, not used
            %hn1 = fminbnd(@(h) NormalFlow(z,b,n,s,q,h),0,10);
            [hn1]=NormalDepth(q,n,B,m,Sb,Y0);
            NormLoc=0;
            
            %Determining where the point is where normal flow ends.
            %Think this over of it should not be rather simple
            Counter=0;
             for wnum=length(HinitH):-1:1
                 if HinitH(wnum)<hn1+0.01 && Counter==0
                   NormLoc=wnum;
                   Counter=1;
                 end
             end
            OneSection=L/(length(HinitH)-1);
            NormLength=(NormLoc-1)*OneSection;
            BwLength=L-NormLength;
            a2=BwLength*t;

a3=L*t;

%%
 [k1 p21_mag p21_phase_corr p22_mag p22_phase_corr,p11_mag, p11_phase_corr, p12_mag, p12_phase_corr  ]=Bode_fun(m, B, Sb, n,q, L, Y0,0,0,0,0);
%  
%%  
[p11_inf, p12_inf, p21_inf, p22_inf, au, ad, tu, td, yn, yu]=IdzFun(q,n,B,m,Sb,Y0,L);
Startexp=0;
Finexp=0;
TimePoints=0;
SpacePoints=50;
[freq, p21_mag_dist, p22_mag_dist, p11_mag_dist, p12_mag_dist, appra11, appra12, appra21, appra22 ]=BackwaterAreaApproximation(m, B, Sb, n,q, L, Y0, Startexp, Finexp, TimePoints,SpacePoints);

sysid1=tf([1],[a1 0]);
sysid2=tf([1],[a2 0]);
%%
sysid3=tf([1],[a3 0]);
sysid4=tf([1],[ad 0]);
sysid5=tf([1],[appra21(end) 0]);




 syslist={sysid1 sysid2 sysid3 sysid4 sysid5} ;
discretization_numbers=500;
magnitude_models=zeros(discretization_numbers,length(syslist));
phase_models=zeros(discretization_numbers,length(syslist));

for k=1:length(syslist)
[mag,pha,w_d] = bode(syslist{k},logspace(-4,-1,discretization_numbers));
magnitude_models(:,k)=squeeze(mag(1,1,:));
phase_models(:,k)=squeeze(pha(1,1,:));
end
%%
figure

for k=1:length(syslist)
semilogx(w_d,20*log10(magnitude_models(:,k)))
hold on

end
semilogx(k1,p21_mag,'k')
semilogx(k1,p11_mag,'b')

legend('Lit', 'My', 'whole', 'idz',  'sv')
grid



