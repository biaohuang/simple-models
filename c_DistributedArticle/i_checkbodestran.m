clear 
close all
addpath('..\Final')
tic
%The physical data of the reach

NodeNumGiven=7;
% 
Sb=0.0015;
L=3000;
B=3;
m=0;
n=0.03;
Y0=3;
q=1.5;

% Y0=1;
% Sb=0.001;

Startexp=-5.5;
Finexp=-1;
TimePoints=2000;
SpacePoints=0;
[ HinitH, distance] = SteadyState(L, m,B,n,Sb,q,Y0,50);

[freq, p21_mag, p21_phase_corr, p22_mag, p22_phase_corr, p11_mag, p11_phase_corr, p12_mag, p12_phase_corr ]=...
     Bode_fun(m, B, Sb, n,q, L, Y0, Startexp, Finexp, TimePoints,SpacePoints);
