function [ HinitH, distance] = SteadyState(L, z,Bw,n,s,q,h,NodeNum, option_chezy)
%SteadyState Profile
%Klaudia Horv�th, Delft, 2016.12.21.
%Modified: 2017.01.15.
switch nargin
    case 8
        option_chezy=1;
    otherwise
        c = 0;
end


% %input variables
% SimTime=8640; %in seconds
% s=0.00005;
% L=4225;
% Bw=8.71;
% z=0;
% n=0.02;
% dt=15;
% SimLength=floor(SimTime/dt)+1;
% qu=ones(1,SimLength+1)*2;
% qu(2:floor(3600/dt))=12;
% qd=ones(1,SimLength+1)*2;
% h=3.56;
% NodeNum=10;

% output Time,Hm2


%option_chezy=1;



%Rename the t subscript for water levels to h
%Think on putting indices to all variables
% First step: known variables
DischargeUpstreamBoundary=1;
DischargeDownstreamBoundary=1;
%NodeNum=165;

%SimTime=1; %in seconds


QNum=NodeNum-1;

%Geometry
Sb=s;
Elevation=L*s;
d=-linspace(Elevation,0,NodeNum);

%Q=ones(1,QNum)*qd(1);
dxf=ones(1,QNum)*L/QNum;
% dxf(1)=dxf(2)/2;
% dxf(QNum)=dxf(2)/2;
dxh=[dxf(2)/2  dxf(2:end) dxf(2)/2];
%dxh=[dxf(2)  dxf(2:end) dxf(2)];


g=9.81;
T=Bw+z*h*2;
a=(Bw+T)*h/2;
P=Bw+((T-Bw)^2/4+h^2)^0.5*2;
ch=1/n*(a/P)^(1/6);

ProfFactor=1;
%n=1/ch/h^(1/6);

%q=2;
%s=-(d(1)-d(end))/L;
%qu(10:end)=3;
%% Initial profile
dx=dxf(2);
NodeNumProfile=(NodeNum*2-1-1)*ProfFactor+1;
dxProfile=dx/2/ProfFactor;
%hn = fminbnd(@(h) NormalFlow(z,Bw,n,s,q,h),0,10);

for i=1:NodeNumProfile
     T=Bw+z*h*2;
     a=(Bw+T)*h/2;
     P=Bw+((T-Bw)^2/4+h^2)^0.5*2;
     R=a/P;
     fr=((q^2*T)/(g*a^3))^0.5;
     chm(i)=1/n*R^(1/6);
     if option_chezy==1
     Sf=q^2*(1/ch)^2/(a^2*R^(1));
     else
     Sf=q^2*(n)^2/(a^2*R^(4/3));
     end
     dydx=(s-Sf)/(1-fr^2);
     hnew=h-dxProfile*dydx;
     hmat(i)=h;
     h=hnew;
     distance(i)=L-(i-1)*dxProfile;
end
HinitH=hmat(end:-ProfFactor:1);
