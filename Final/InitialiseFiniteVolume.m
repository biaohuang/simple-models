function [ Time,Hm2 , Hm,HinitH] = InitialiseFiniteVolume(L, z,Bw,n,s,SimTime,dt,qu,qd,h,NodeNum )


SimLength=floor(SimTime/dt)+1;
quini=ones(1,SimLength+2)*qd(1);
qdini=ones(1,SimLength+2)*qd(1);

DesiredStartLevel=h;
StartWaterLevel=h;
AchievedWaterLevel=DesiredStartLevel*2;
counter=0;
%This is running the the model, including a steady state initialisation,
%and notes which level to start from to have the final desired profile
for k=1:7
    if abs(AchievedWaterLevel-DesiredStartLevel)> 0.00005
        counter=counter+1;
        %First we make a run that definitely starts from the given level
        %[ ~,Hm2 , ~,~] =FiniteVoumeModel11bck(L, z,Bw,n,s,3600*18,dt*10,quini,qdini,StartWaterLevel,NodeNum, 0, 0 );
        [ ~,Hm2 , ~,~] =FiniteVoumeModel11bck(L, z,Bw,n,s,SimLength*10*dt,dt*10,quini,qdini,StartWaterLevel,NodeNum, 0, 0 ); %mod 10.24.

        AchievedWaterLevel=Hm2(end,end);
        if AchievedWaterLevel>DesiredStartLevel
            StartWaterLevel=StartWaterLevel-(AchievedWaterLevel-DesiredStartLevel)*1;
        elseif AchievedWaterLevel<DesiredStartLevel
            StartWaterLevel=StartWaterLevel+(-AchievedWaterLevel+DesiredStartLevel)*1;
        end
    end
end
%%
[ Time,Hm2 , Hm,HinitH] =FiniteVoumeModel11bck(L, z,Bw,n,s,SimTime,dt,qu,qd,h,NodeNum, 3, 0 );
