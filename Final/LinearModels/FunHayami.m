function [aa, bb, cc, dd, ee, r, cl, yn, ccnoni, ddnoni, eenoni, G, K1, tau, ae]=FunHayami(q,n,B,m,Sb,Y0,L,kh,dt)
%The input variables are the following:
% q -discharge
% n - Manning's coefficient
% B - bottom width
% m - side slope
%Sb - bottom slope
%Y0 - water depth
%L  - length of the canal
%kh - nothing, not used
%dt - sampling time, in case of using discretized results

% output
% the last three output is needed
%K1 is a constant for short canals, and a vector containing k1 and k2 for
%long canals
%ae is the backwater area



% %It gives other category'
% % c is too small
% clear all
% q=1.9;
% n=0.02;
% B=3;
% m=0;
% Sb=0.0003;
% Y0=1.9;
% L=7000;
% kh=0;
% dt=10;             
% % 
% q=1.5;
% Y0=1;




kh=0;
sb=Sb;
Man=n;
b0=B;
h=Y0;
yx=h;

%Calculationg the divison points in case of other canals-------------------


g=9.81;



kh=0;       %initial height = 0
sb=Sb;      %bottom slope
Man=n;      %Manning coefficient??
b0=B;       %Bottom width
h=Y0;       %initial height??
yx=h;       %downstream height

%Calculationg the divison points in case of other canals-------------------

g=9.81;

    %Normal depth
    
    y1=0;           %initial values??
    y2=yx*5;        
    k=0;
 for k=1:25          
      y=(y1+y2)/2;      %average height
      a=b0*y+m*y^2;     %using average height to calculate area     
      p=b0+2*y*(1+m^2)^0.5;     %wet perimeter calculated trapezoidal cross section
      r=a/p;                    %hydrolic radius
      dif=(q^2*n^2)/(a^2*r^(4/3))-sb;       %partial derivative of the friction slope?? sb = bottom slope
      if dif<0
          y2=(y1+y2)/2;
      else
          y1=(y1+y2)/2;
      end
    end
    yn=y;       %Normal depth


% x1=0;
% x2=(L+x1)/2;
% sxx=(sb-sf0(yx,q,n,b0,m))/(1-(froude(yx,q,n,b0,m))^2);
% y2=yx-(L-x2)*sxx; %good


     sxx=(sb-sf0(yx,q,n,b0,m))/(1-(froude(yx,q,n,b0,m))^2); 
     yu=yx-(L)*sxx; %good

    if sxx==0
        x1_calc=L;
    else
        x1_calc=max(0,L-(yx-yn)/sxx);
    end
    x1=x1_calc;
    %y1
    if x1==0
        y1=yx-sxx*L;
    else
        y1=yn;
    end
    %x2
    x2=(L+x1)/2;
    %y2
    %y2=y1+(L-x1)*sb;
    %For backwater
    
    if x1==0
       y2=yx-(L-x2)*sxx;
    else
      y2=yn+(x2-x1)*sxx;
    end
    
    


%y2=yx;
Lbw=L-x1;
Lunif=x1;



% 
% % Part 3 ----Downstream part------------------------------------------------
% 
 a=b0*y2+m*y2^2;
% p=b0+2*y2*(1+m^2)^0.5;
% r=a/p;
 t=b0+2*y2*m;
 c=(g*a/t)^0.5;
 v=q/a;
 
 tau=Lunif/(c+v)/2+Lunif/(c-v)/2;
 ae=Lbw*t;

 
 hn=y2;
 b=b0;
 z=m;
 s=Sb;
 l=L;
 
 qmatr=ones(100,1)*q;
 
 
 
%  [ Timex,Hm2x , Hmx,HinitH] = FiniteVoumeModel11bck(l, z,b,n,s,1,dt,qmatr,qmatr,Y0,16,  0, 0 );
  Y0old=Y0;
  NodeNum=16;
     dxProfile=L/(NodeNum-1)/2;
  NodeNumProfile=(NodeNum*2-1-1)*1+1;
T=b+z*Y0*2;
     a=(b+T)*Y0/2;
     P=b+((T-b)^2/4+Y0^2)^0.5*2;
     %R=a/P;
ch=1/n*(a/P)^(1/6); %MOD 04.05
for i=1:NodeNumProfile
     T=b+z*Y0*2;
     a=(b+T)*Y0/2;
     P=b+((T-b)^2/4+Y0^2)^0.5*2;
     R=a/P;
     fr=((q^2*T)/(9.81*a^3))^0.5;
     %ch=1/n*R^(1/6);
     Sf=q^2*(1/ch)^2/(a^2*R^(1));
     dydx=(s-Sf)/(1-fr^2);
     hnew=Y0-dxProfile*dydx;
     hmat(i)=Y0;
     Y0=hnew;
end
HinitH=hmat(end:-1*2:1);
Y0=Y0old;
  
  
            %Normal depth, not used
            %hn1 = fminbnd(@(h) NormalFlow(z,b,n,s,q,h),0,10);
            [hn1]=NormalDepth(q,n,b,m,Sb,Y0);
            NormLoc=0;
            
            %Determining where the point is where normal flow ends.
            %Think this over of it should not be rather simple
             for wnum=length(HinitH):-1:1
                 if HinitH(wnum)<hn1+0.01
                   NormLoc=wnum;     
                 end
             end
            OneSection=L/(length(HinitH)-1);
            NormLength=(NormLoc-1)*OneSection;
            BwLength=L-NormLength;

            %hn=HinitH(floor(length(HinitH)/2));
            hn= HinitH(floor(NormLoc+(length(HinitH)-NormLoc)/2));
            
            
            
            
 
 
             %Calculation of the parameters
            t=b+z*hn*2;                                             %calculation T = B+2zy
            a=(b+t)*hn/2;                                           %
            v=q/a;                                                  %    
            fr=((q^2*t)/(g*a^3))^0.5;                               %Froude number %Cannot be higher than 1
            hc=v^2/g;                                               %
            p=b+2*((t-b)^2/4+hn^2)^0.5;                             %        
            r=a/p;                                                  %    
            ch=1/n*r^(1/6); %Chezy                                  %    
            sf=q^2*n^2/(a^2*r^(4/3));                               %Friction Slope *Manning equation    
            dydx=(s-sf)/(1-fr^2);                                   % 

            
            
            chi0=s*l/hn; %Original                   equations 1 and 2 for normal flow
            eta0=chi0/(fr*(1-fr)); %Original
            
            %for rectangular channels (equation (4)
            %Sb*Xr*(g/(v*(sqrt(g*yr)-v))
            
            clit=5*q/(3*b*hn);  %Litrico parameter   Rectangular channels
            dlit=q/(2*b*sf);    %Litrico parameter   Rectangular channels
            %Above formulas for normal flow
            
            
            
            
            dme=q/(2*t*sf);     %Litrico parameter not only for normal flow
            y=hn;

            p1=sf*(-10/3*(b+2*z*hn)+8/3*r*(1+z^2)^0.5)/a;
            p2=2*z;
            p3=1; %But finally we approximate dy/dz as 1

            %cbrot=(q/(2*b^2*sf))*10*b*sf/3/a*b; %C according to Brotons
            cme=q/(2*t^2*sf)*(2*z*dydx-2*z*sf-t*(p1)); %This is the C from Litrico's paper, calculate by hand
             cl=cme*l/dme/2;
 %%           
 if cl<1
     G=1;
     K1=l/cme;
     tau=0;
 elseif cl<9/4
     G=1;
     K1=(2*l*dme/cme^3)^0.5;
     tau=(l/cme)-K1;
 else
     d=(4*l^2*dme^3/cme^9)*(9*dme/cme-2*l);
     b=6*l*dme^2/(cme^5);
     if d<0
         fi=pi/2+atan(b/(abs(d))^0.5)    ;
         rho=(b^2+abs(d))^0.5;
         S=2*rho^(1/3)*cos(fi/3);
     else
         S=(-b+d^0.5)^(1/3)+(-b-d^0.5)^(1/3);
     end
     G=1;
     P=(2*l*dme/cme^3)*(1-3*dme/S/cme^2);
     K(1)=0.5*(S+1i*(4*P-S^2)^0.5);
     K(2)=0.5*(S-1i*(4*P-S^2)^0.5);
     K1=K;
     tau=l/cme-S;
     r=floor(tau/dt);
     ll=tau-r*dt;
     for k=1:2
         z(k)=exp(-dt/K(k));
         alpha(k)=G*(1-exp((ll-dt)/K(k)));
         beta(k)=G*z(k)*(exp(ll/K(k))-1);
         p(k)=-1/K(k);
         
         
         %    alpha(k)= G*((1-exp((ll-dt)/K(k)))*(1-K(k)/dt)+1-ll/dt);
         %    beta(k)=G*((2*exp((ll-dt)/K(k))-1)*(1-K(k)/dt)+((K(k)+ll)/dt-2)*zi+ll/dt);
         %    gamma(k)=G(zi*(1-(K(k)+ll)/dt)-exp((ll-dt)/K(k))*(1-K(k)/dt));
         
     end
     aa=sum(z);
     bb=z(1)*z(2);
     cc_=(p(1)*alpha(2)-p(2)*alpha(1))/(p(1)-p(2));
     dd_=(p(2)*alpha(1)*z(2)-p(1)*alpha(2)*z(1)+p(1)*beta(2)-p(2)*beta(1))/(p(1)-p(2));
     ee_=(p(2)*beta(1)*z(2)-p(1)*beta(2)*z(1))/(p(1)-p(2));
     
     
 end
 if cl<9/4
     r=floor(tau/dt);
     ll=tau-r*dt;
     aa=exp(-dt/K1);
     cc_=G*(1-exp((ll-dt)/K1));
     dd_=G *exp(-dt/K1)*(exp(ll/K1)-1);
     bb=0;
     ee_=0;
 end
% Q_{out}(k+1)=aQ_{out}(k)+cQ(k-r-1)_{in}+bQ(k-r)_{in}            
             
%    for p=1:2
%     z(p)=exp(-Te/K(p))
%     a_(p)=G*(1-exp((L-Te)/K(p)))
%     b_(p)=G*z(p)*exp((L/K(p))-1)
%     pi(p)=-1/K(p)
% end

 
%Make it for w depth c,d,e
%ae=BwLength;
cc=cc_*dt/ae;
dd=dd_*dt/ae;
ee=ee_*dt/ae;

%For distributed

ccnoni=cc_*dt;
ddnoni=dd_*dt;
eenoni=ee_*dt;



 