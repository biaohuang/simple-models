function [x Aout Bout D,Hinit]=LinearInertialImp(z,b,h0,n,q0,s,l,dt,NodeNumGiven,ModelType)
%clear
EndTime=864000;
UpstreamBoundaryQ=1;
DownstreamBoundaryQ=1;
g=9.81;
% ModelType=2; %LinearizedInertial
% ModelType=1; %Lineareized SV
% ModelType=3; %Linear model 

% NodeNumGiven=8;
% dt=100;
% ModelType=2;
% z=2;%one step up how much widens
% b=3;
% h0=3;
% n=0.02;
% q0=1;%+0.125;
% s=0.0003;
% l=7000;
%[x, A, Bd, D,Hinit]=LinearInertial(z,b,h0,n,q0,s,l,dt,NodeNumGiven,ModelType);
%%
% n=0.04;
% s=0.00002;
% q0=0.5;
% h0=3;
% l=16000;
% b=7;
% z=4.3;
% dt=50;

%%
h=h0;
q=q0;

%Discretization
%dt=50;
ProfFactor=100; %Set this how denser you will calculate the initial profile than the actual later calculation
%NodeNumGiven=12; %

if DownstreamBoundaryQ==1  %The final node number depends on the boudnary conditions
  NodeNum=floor(NodeNumGiven/2)*2-1;  
else
  NodeNum=floor(NodeNumGiven/2)*2;  
end
%Here if we use  q q boundary, the number of nodes should be odd
%Maybe we could do with an adjsutment
%In cae of even node num:
%In case of odd node num: NodeNumProfile=(NodeNum-1)*ProfFactor+1;
%NodeNumProfile=NodeNum*ProfFactor;
NodeNumProfile=(NodeNum-1)*ProfFactor+1;
dx = l/(NodeNum-1);
dxProfile=dx/ProfFactor;

%Calculation of the Courant Number
T=b+z*h*2;
a=(b+T)*h/2;
C=(g*a/T)^0.5;
Cr=(dx/dt)^(-1)*C;

T=b+z*h*2;
    % a=(Bw+T)*h/2;
     P=b+((T-b)^2/4+h^2)^0.5*2;
     %R=a/P;
ch=1/n*(a/P)^(1/6); %MOD 04.05

%% Initial profile
for i=1:NodeNumProfile
    T=b+z*h*2;
    a=(b+T)*h/2;
    P=b+((T-b)^2/4+h^2)^0.5*2;
    R=a/P;
    fr=((q^2*T)/(g*a^3))^0.5;
    C=(g*a/T)^0.5;
    Sf1=q^2*n^2/(a^2*R^(4/3));
    %ch=1/n*R^(1/6); commented 04.05.
    Sf=q^2*(1/ch)^2/(a^2*R^(1)); %Mod 04.05.
%    Sf=q^2*(1/ch)^2/(a^2*h^(1));

    V=q/a;
    dydx=(s-Sf)/(1-fr^2);
    %hld=h;
    hnew=h-dxProfile*dydx;
    hmat(i)=h;
    h=hnew;
end
Hinit=hmat(end:-ProfFactor:1);

% %% Plotting the initial profile
% 
% a1=0:length(hmat)-1;
% a2=a1(1:ProfFactor:end);
% figure(11)
% plot(a2*dxProfile/1000,Hinit,'*')
% hold on
% plot(a1*dxProfile/1000,hmat(end:-1:1),'^r')
% xlabel('Chainage (km)')
% axis tight
% title('Initial steady state water depth')
%% Calculating the paramters for the matrix depending on water depth
for i=1:NodeNum
    h=Hinit(i);
    T=b+z*h*2;
    a=(b+T)*h/2;
    P=b+((T-b)^2/4+h^2)^0.5*2;
    R=a/P;
    fr=((q^2*T)/(g*a^3))^0.5;
    C=(g*a/T)^0.5;
    Sf=q^2*n^2/(a^2*R^(4/3));
    %ch=1/n*R^(1/6);  %commented 04.05.
    Sf=q^2*(1/ch)^2/(a^2*h^(1));
    Sf=q^2*(1/ch)^2/(a^2*R^(1));
    %alpha=C^2*T;
    V=q/a;
    %alpha=(C^2-V^2)*T;
    alpha=g*a;
    rho=2*g/V*Sf;
    dpdy=((T-b)^2/4+h^2)^(-0.5)*2*h;
    kappa=7/3-(4*a)/(3*T*P)*(dpdy); %Here the last term is specific for trap
    dydx=(s-Sf)/(1-fr^2);
    dtdx=2*z;
    gamma1=V^2*dtdx+g*T*(kappa*Sf+s-(1+2*fr)*dydx);
    gamma2=g*T*(kappa*Sf+s-dydx);
    gamma3=g*T*(s-dydx);
    if ModelType==1
        gamma=gamma1;
    elseif ModelType==2
        gamma=gamma2;
    elseif ModelType==3
        gamma=gamma3;
    end
    
    if ModelType==1
    rho=2*g/V*Sf;
    elseif ModelType==2
    rho=2*g/V*Sf;
    elseif ModelType==3
    rho=g*q/(a*ch^2*R);
    end
    
    if ModelType==1
       mu=2*V;
    elseif ModelType==2
       mu=0; %If mu=0 then it is the inertial model
    elseif ModelType==3
       mu=0; %If mu=0 then it is the inertial model
    end
    %gamma=g*T*(kappa*Sf+s-dydx); %it s not clear here...

    
    TM(i)=T;
    alphaM(i)=alpha;
    gammaM(i)=gamma;
    rhoM(i)=rho;
    muM(i)=mu;
    T0=T;
    alpha0=alpha;
    gamma0=gamma;
    rho0=rho;
    

end
%save vars2 gamma1 gamma2 gamma3 gammaM gamma
%% Filling in the matrix
%Parameters: the locaiton of the parameters is the following: they are
%numbered as 2 steps longer than the matrix, on before and one after. So
%for hte first column of the matrix parameter 2 should be used, etc.

MatrixSize=NodeNum-2;
A=zeros(MatrixSize,MatrixSize);
Bd=zeros(MatrixSize,2);
C=zeros(1,MatrixSize);
C(1,end)=1;
%Bd first rows
Bd(1,1)=-dt/(2*TM(1)*dx); 
Bd(2,1)=dt*muM(1)/(4*dx);
%Bd last rows
if DownstreamBoundaryQ==0
%Bd(MatrixSize,2)=-alphaM(MatrixSize+2)*dt/2/dx+dt*gammaM(MatrixSize+2)/2; %mod 11.18.
elseif DownstreamBoundaryQ==1
%Bd(MatrixSize-1,2)=-muM(MatrixSize+2)*dt/4/dx;
Bd(MatrixSize,2)=dt/(TM(MatrixSize+2)*2*dx); 
end
%A First row
A(1,1)=1;
A(1,2)=dt/(TM(3)*2*dx);

%A Last
if DownstreamBoundaryQ==0
%Last but one row
A(MatrixSize-1,MatrixSize-2)=dt/(TM(MatrixSize-1)*2*dx);
A(MatrixSize-1,MatrixSize-1)=1;
A(MatrixSize-1,MatrixSize)=-dt/(TM(MatrixSize+1)*2*dx); 
%Last
A(MatrixSize,MatrixSize-2)=dt*muM(MatrixSize-1)/(2*dx);
A(MatrixSize,MatrixSize-1)=alphaM(MatrixSize)*dt/2/dx+dt*gammaM(MatrixSize)/2;%mod 11.18.
A(MatrixSize,MatrixSize)=1-rhoM(MatrixSize+1)*dt-dt*muM(MatrixSize+1)/(2*dx);
EndOfCycle=MatrixSize/2-1;  
elseif DownstreamBoundaryQ==1
%Last but one row
%A(MatrixSize-1,MatrixSize-3)=dt*muM(MatrixSize-2)/(2*dx);
A(MatrixSize-1,MatrixSize-2)=-alphaM(MatrixSize-1)*dt/2/dx-dt*gammaM(MatrixSize-1)/2;%mod 11.18.
A(MatrixSize-1,MatrixSize-1)=1+rhoM(MatrixSize)*dt;%-dt*muM(MatrixSize)/(2*dx);
A(MatrixSize-1,MatrixSize)=alphaM(MatrixSize-1)*dt/2/dx-dt*gammaM(MatrixSize-1)/2;
%Last row
A(MatrixSize,MatrixSize-1)=-dt/(TM(MatrixSize)*2*dx);
A(MatrixSize,MatrixSize)=1;
EndOfCycle=(MatrixSize-1)/2-1;  
end
    
%%

for i=1:EndOfCycle
    %Discharges,even
    A(2*i,2*i-1)=-alphaM(2*i-1+1)*dt/2/dx-dt*gammaM(2*i-1+1)/2;%mod 11.18.
    A(2*i,2*i)=1+rhoM(2*i+1)*dt; %mod from i %I am not sure... 11.16.
    A(2*i,2*i+1)=alphaM(2*i+1+1)*dt/2/dx-dt*gammaM(2*i+1+1)/2;%mod 11.18.
%     if DownstreamBoundaryQ==0
%     A(2*i,2*i+2)=-muM(2*i+3)*dt/4/dx;
%     end
% 
%     if i>1
%       A(2*i,2*i-2)=muM(2*i-1)*dt/4/dx;
%     end
    
    %Water levels, odd
    A(2*i+1,2*i)=-dt/(TM(2*i+1)*2*dx);
    A(2*i+1,2*i+1)=1;
    %if DownstreamBoundaryQ==0
    A(2*i+1,2*i+2)=dt/(TM(2*i+2+1)*2*dx); 
    %end
end
%% Initialization
hb=0;
qb=0;
D=[qb;hb];
h00=0;
q00=0;
hinit=ones(1,NodeNum+1)*h00;
qinit=ones(1,NodeNum+1)*q00;
xinit=zeros(1,MatrixSize);
% xinit=[];
% %Initialize with equal q an h everywhere.
% for i=1:MatrixSize/2
%   xinit=[xinit hinit(i) qinit(i+1)]  ;
% end
x=xinit';

Aout=inv(A);
Bout=-inv(A)*Bd;