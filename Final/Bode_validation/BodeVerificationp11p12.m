%Verification Bode for p21 and p22
%Litrico canal1
close all
clear

p11_mag_litrico=csvread('Data\nofric\p11_mag_nofric.csv');
p11_phase_litrico=csvread('Data\nofric\p11_phase_nofric.csv');
p12_mag_litrico=csvread('Data\nofric\p12_mag_nofric.csv');
p12_phase_litrico=csvread('Data\nofric\p12_phase_nofric.csv');

%%
x=3000;
m=1.5;
B=7;
Sb=1e-4;
Sb=0;
n=0.02;
n=0;
Y0=2.12;
q=14;

Startexp=-3.5;
Finexp=-1.9;
TimePoints=0;
SpacePoints=0;   %Number of discretization points%The choice will influence the distance

%-----------------------------------------------------------------

cd ..             
 [freq, p21_mag, p21_phase, p22_mag, p22_phase, p11_mag, p11_phase, p12_mag, p12_phase ]=...
     Bode_fun(m, B, Sb, n,q, x, Y0, Startexp, Finexp, TimePoints,SpacePoints);             
cd Bode_validation

%% Plotting
close all
my_width=1;


figure(6)

subplot(2,1,1)
semilogx(p11_mag_litrico(:,1),p11_mag_litrico(:,2),'r','LineWidth',my_width)
hold on
semilogx(freq,p11_mag,'b','LineWidth',my_width)
grid
xlabel('freq. (rad/s)')
ylabel('gain (dB)')
xlim([min(freq) max(freq)])
title('p11')

subplot(2,1,2)
semilogx(p11_phase_litrico(:,1),p11_phase_litrico(:,2),'r','LineWidth',my_width)
hold on
semilogx(freq,p11_phase,'b','LineWidth',my_width)

grid
xlabel('freq. (rad/s)')
ylabel('phase (dg)')
xlim([min(freq) max(freq)])


figure(8)

subplot(2,1,1)
semilogx(p12_mag_litrico(:,1),p12_mag_litrico(:,2),'r','LineWidth',my_width)
hold on
semilogx(freq,p12_mag,'b','LineWidth',my_width)
title('p12')
grid
xlabel('freq. (rad/s)')
ylabel('gain (dB)')
xlim([min(freq) max(freq)])

subplot(2,1,2)
semilogx(p12_phase_litrico(:,1),p12_phase_litrico(:,2),'r','LineWidth',my_width)
hold on
semilogx(freq,p12_phase,'b','LineWidth',my_width)

grid
xlabel('freq. (rad/s)')
ylabel('phase (dg)')
xlim([min(freq) max(freq)])



