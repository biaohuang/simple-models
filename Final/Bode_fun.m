function [freq, p21_mag, p21_phase_corr, p22_mag, p22_phase_corr, p11_mag, p11_phase_corr, p12_mag, p12_phase_corr ]=Bode_fun(m, B, Sb, n,q, x, Y0, Startexp, Finexp, TimePoints,SpacePoints)

%Frequency approximation of open channel flow - Litrico
%Litrico, Xavier, and Vincent Fromion. "Frequency modeling of open-channel flow." Journal of Hydraulic Engineering 130.8 (2004): 806-815.
%2012.05.07 Working
%2017.06.28. Making it to be a function
%The first output is the frequency in rad/s. The gain values are output in decibels and the phase values in degrees.

 g=9.81;

%-----------------------------------------------------------------
if Startexp==0
    Startexp=log10((g*Y0)/(2*x))-1;
end
    
if Finexp==0
    Finexp=log10((g*Y0)/(2*x))+1;
end

if TimePoints==0
    TimePoints=1000;
end
if SpacePoints==0
    SpacePoints=floor(x/5);
end


n_dis=SpacePoints;   %Number of discretization points
             %The choice will influence the distance
             
             
             
hk=x/n_dis;  %Distance of one discretized section

 
 eta=[0;0];  %Starting value for the calculation, [relative q relative h]
 
 yn=real(eta(2))+Y0;  %Calculation of the absolute water level
 k1=logspace(Startexp,Finexp,TimePoints); %Defining the axis of hte Bode plot
 len=length(k1);          
for k2=1:len    
    s=1i*k1(k2);
    for k=1:n_dis

        q_0=eta(1)+q;
        Y0=yn;
        TWidth=B+m*yn*2;
        WArea=(B+TWidth)*yn/2;
        WPerimeter=((m*yn)^2+yn^2)^0.5*2+B;
        HRadius=WArea/WPerimeter;
        V0=q_0/WArea;
        C0=(g*WArea/TWidth)^0.5;
        F0=V0/C0;
        Sf=q_0^2*n^2/(WArea^2)/(HRadius^(4/3));
        dYdX=(Sb-Sf)/(1-F0^2);
        dPdY=2*(m^2+1)^0.5;
        kappa=7/3-4*WArea/(3*TWidth*WPerimeter)*dPdY;
        gamma=g*TWidth*((1+kappa)*Sb-(1+kappa-(kappa-2)*F0^2)*dYdX);
        beta=-2*g/V0*(Sb-dYdX);

        As11=0;
        As12=-TWidth*s;
        As21=(-s+beta)/(TWidth*(C0^2-V0^2));
        As22=(2*V0*TWidth*s+gamma)/(TWidth*(C0^2-V0^2));
        As=[As11 As12; As21 As22];

         if k==1
                A=As*hk;
                matr=((eye(2)+A+0.5*A^2+1/6*A^3+1/24*A^4+1/96*A^5+1/(6*96)*A^6+1/(6*96*7)*A^7));
         else
                matr=((eye(2)+A+0.5*A^2+1/6*A^3+1/24*A^4+1/96*A^5+1/(6*96)*A^6+1/(6*96*7)*A^7))*matr;
         end
         ga11=matr(1,1);
         ga12=matr(1,2);
         ga21=matr(2,1);
         ga22=matr(2,2);

         p11=-ga11/ga12;
         p12=1/ga12;
         p21=ga21-ga22*ga11/ga12;
         p22=ga22/ga12;

         eta=((eye(2)+A+0.5*A^2+1/6*A^3+1/24*A^4+1/96*A^5+1/(6*96)*A^6+1/(6*96*7)*A^7))*eta;
         yn=(eta(2))+Y0;

    end
    p11_mag(k2)=20*log10(abs(p11));
    %p11_phase(k2)=1/3.14*180*(atan(imag(p11)/real(p11)));
    p11_phase(k2)=1/pi*180*(atan2(imag(p11),real(p11)));

    
    p12_mag(k2)=20*log10(abs(p12));
    %p12_phase(k2)=1/3.14*180*(atan(imag(p12)/real(p12)));
    p12_phase(k2)=1/pi*180*(atan2(imag(p12),real(p12)));

    
    p21_mag(k2)=20*log10(abs(p21));
    %p21_phase(k2)=1/3.14*180*(atan(imag(p21)/real(p21)));
    p21_phase(k2)=1/pi*180*(atan2(imag(p21),real(p21)));

    
    p22_mag(k2)=20*log10(abs(p22));
    %p22_phase(k2)=1/3.14*180*(atan(imag(p22)/real(p22)));
    p22_phase(k2)=1/pi*180*(atan2(imag(p22),real(p22)));


end

%Phase correction--------------------------------------------------


n=length(p21_phase);
p11_phase_corr=p11_phase;
p12_phase_corr=p12_phase;
p21_phase_corr=p21_phase;
p22_phase_corr=p22_phase;
threshold_difference=179;
threshold_difference_up=179;
threshold_difference_down=300;
for i1=2:n
  if (p11_phase_corr(i1)-p11_phase_corr(i1-1))  >threshold_difference
      p11_phase_corr(i1:n)=p11_phase_corr(i1:n)-360;
  end
  
  if (p11_phase_corr(i1)-p11_phase_corr(i1-1))  <-threshold_difference
      p11_phase_corr(i1:n)=p11_phase_corr(i1:n)+360;
  end
  
  if (p12_phase_corr(i1)-p12_phase_corr(i1-1))  >threshold_difference_up
      p12_phase_corr(i1:n)=p12_phase_corr(i1:n)-360;
  end
  
  if (p12_phase_corr(i1)-p12_phase_corr(i1-1))  <-threshold_difference_down
      p12_phase_corr(i1:n)=p12_phase_corr(i1:n)+360;
  end
  
  if (p21_phase_corr(i1)-p21_phase_corr(i1-1))  >threshold_difference
      p21_phase_corr(i1:n)=p21_phase_corr(i1:n)-360;
  end
  
  if (p21_phase_corr(i1)-p21_phase_corr(i1-1))  <-threshold_difference
      p21_phase_corr(i1:n)=p21_phase_corr(i1:n)+360;
  end
  
  if (p22_phase_corr(i1)-p22_phase_corr(i1-1))  >threshold_difference
      p22_phase_corr(i1:n)=p22_phase_corr(i1:n)-360;
  end
  
  if (p22_phase_corr(i1)-p22_phase_corr(i1-1))  <-threshold_difference
      p22_phase_corr(i1:n)=p22_phase_corr(i1:n)+360;
  end
  
end
freq=k1;