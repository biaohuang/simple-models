%Testing the steady state profile

clear 
close all
%addpath('\..\..\..\SimpleModels')
addpath('../../Final')

%input variables
SimTime=3500; %in seconds
s=0.0015;
L=7000;
Bw=3;
z=0;
n=0.03;
dt=10;
SimLength=floor(SimTime/dt)+1;
qu=ones(1,SimLength+1)*1.5;
qd=ones(1,SimLength+1)*1.5;
q=1.5;
h=3;
NodeNum=1000;
UseInitialization=0;
HinitGiven=0;

%Steady state
[yn]=NormalDepth(q,n,Bw,z,s,h);
[ HinitH, distance] = SteadyState(L, z,Bw,n,s,q,h,NodeNum,1 );

%SV %Calculated using NodeNum=1000; and dt=10; 
 [ Time,Hm2 , Hm,x] = FiniteVoumeModel11bck(L, z,Bw,n,s,SimTime,dt,qu,qd,h,NodeNum, UseInitialization, HinitGiven );

%Sobek
sobek=csvread('WaterDepthSobekProfileCh.csv',1,0);

%Plotting
figure('rend','painters','pos',[10 10 1300 600])
MyFont=14;
plot(distance',HinitH(end:-1:1),'LineWidth',2)
hold on
plot(sobek(:,1),sobek(:,2),'.r','MarkerSize',12)
plot(0:7.007007007007007:7000,Hm2(end,:),'--k')
legend({'Steady State','Sobek','Saint-Venant solver'},'Location','best', 'FontSize',MyFont)
xlabel('Distance (m)', 'FontSize',MyFont)
ylabel('Water depth (m)', 'FontSize',MyFont)
title('Steady state profile of a channel', 'FontSize',MyFont)


