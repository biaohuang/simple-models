Calculation of the backwater area
=================================

How to use
----------

This file calculates the backwater area for any point in a channel.


Example
^^^^^^^

.. highlight:: Matlab

An example::

    [freq, p21_mag_dist, p22_mag_dist, p11_mag_dist, p12_mag_dist, appra11, appra12, appra21, appra22 ]=BackwaterAreaApproximation(m, B, Sb, n,q, x, Y0, Startexp, Finexp, TimePoints,SpacePoints);

Explanation
-----------

There are two files, BackwaterApproximation and BackwaterApproximationFast. The difference is only one line,
the fast is not computing all the frequency range, it only computes the lowest frequency. 

Implementation
--------------

The implementation uses the formula (Eq. 3.7 from Horváth, Klaudia. "Model predictive control of resonance sensitive irrigation canals." (2013).)

.. image:: images/BackwaterFormula.PNG




