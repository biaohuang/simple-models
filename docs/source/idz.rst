IDZ
===

The IDZ model is implemented based on the paper of Litrico, X., and V. Fromion. "Simplified modeling of irrigation canals for controller design." Journal of irrigation and drainage engineering 130.5 (2004): 373-383.



Testing
-------

It is tested using the values of the above mentioned article. It was able to reproduce the presented numbers.
The numbers form the article are saved in the excel file, and the file ReadDataLitrico.m does the testing.

How to use
----------

It is a Matlab function, that can be called with the following code::

   [p11_inf_hat, p12_inf_hat, p21_inf_hat, p22_inf_hat, au_hat, ad_hat, tu_hat, td_hat, yn, x2]=IdzFun(q,n,B,m,Sb,Y0,L)

where n is the Manning's coefficient, q is the discharge, B is the bottom width, Sb is the bottom slope, Y0 is the downstream water depth and L is the length of the channel.   
The last two outputs are the normal depth and the location where the channel changes between normal flow and backwater. If the whole channel is under backwater this number is zero.

Implementation
--------------

The implementation follows the paper. There is one small thing to notice:

It is slightly different from the paper in order to be able to reproduce the given numbers. This only has effect when the whole canal is under backwater and it is trapezoidal. For the downstream approximation gamma is calculated two ways: taking into account the side slope of the trapezoid and not. (For the upstream it is not relevant, as the water depth does not change.) In order to calculate the frequency approximation the whole formula was used for gamma, and in order to calculate the backwater area the first term (related to the derivative of the top width) was not taken into account.

Distributed version
-------------------

