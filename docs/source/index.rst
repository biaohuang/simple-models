Channel modelling
=================

This is a set of Matlab files to model open water channels for control purposes.

.. toctree::
   :maxdepth: 3

   idz
   bode
   steady
   sv
   backwater
   notes
   idz_distributed
   ir
   inertial



Contribute
----------

- Source Code: github.com/Klaudia/simple-models

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: hklau85@gmail.com

License
-------

The project is licensed under the BSD license.


