Channel modelling
=================

.. toctree::
   :maxdepth: 3

   backwater
   idz
   bode
   steady

Bode plots will solve your problem of making a Bode plot of an open water channel.

.. highlight:: matlab

Look how easy it is to use

::

    [k1 ga ga_phase ga_phase_22 ga_22]=Bode_fun(m, B, Sb, n,q, x, Y0);

Features
--------

- This is a set of Matlab files to model open water chanels

Contribute
----------

- Source Code: github.com/Klaudia/simple-models

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: hklau85@gmail.com

License
-------

The project is licensed under the BSD license.


