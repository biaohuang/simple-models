Frequency plots 
===============

When the channel is under backwater, the backwater area (hence the integrator) is well-estimated. However, when the channel has normal depth upstream, the integrator is estimated differently by different models.

Let's review the estimation of the integrator by different models.
It should be unified. There are two main approaches:
-Use the whole water surface
-Use only the backwater surface. This can be determined by the method of Litrico.

Remember: the integrator is cutting the x-axis (if it is at 1) at 20*log10(1/A), where A is the backwater surface.

