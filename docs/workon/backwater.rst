Backwater approximations
========================

Introduction
------------

Most simple linear models have a part for the approximation of the integrator. The value of the integrator influences the change in water level after the steady state is reached. This effect is different in the upstream and in the downstream part, especially when the channel is not completely under backwater. In that case, there is normal flow condition upstream, and the steady state water level does not change if the discharge is kept constant. Of course, all these approximations are valid in a range around the reference discharge. The more the discharge changes, the bigger error these approximations bring. In case of trapezoidal channels, there is an additional non-linearity present: the backwater surface changes with water depth.

Upstream effect
---------------
The upstream steady state water level change depends on the conditions of the flow: is normal flow present or not. 

- If normal flow is not present, hence the whole channel is under backwater, the water level increases approximately proportional to the backwater surface. In this case thus the backwater surface is the surface of the whole channel. 

- If normal flow is present, the upstream water level does not change. Thus, the value of the integrator is infinite. 

Downstream effect
-----------------

The downstream effect also depends of the presence of normal flow.

- If normal flow is not present, just like in the previous case, the whole channel is affected by backwater, and the backwater surface is the surface of the whole channel.

- If normal flow is present, the case is more complex. The backwater area is smaller, and the water level increase downstream is proportional to that backwater area (with other words the integrator). So the question is how can this area be determined, how long part of the channel is under backwater. If the point of transition between backwater and normal flow is found, then the backwater area can easily be obtained.



Effect in the middle of the channel
-----------------------------------

The first step is to identify if the point belongs to the normal flow part or to the backwater part. If it belongs to the normal flow part, the water level does not change. Thus, the value of the integrator is infinite. If it belongs to the backwater part, the same methods are valid as for the downstream part.

Questions and challenges
------------------------

- Which method to use to determine the boundary between normal flow and backwater? Possible methods are: Litrico method, calculate the point. Also Litrico has a different area approximation.
- How to deal with trapezoidal channels? 

Steps
-----

- Make the bodes for upstream and downstream. 

Make a plan for a document and a story.

Goal: how to approximate best an open water channel as an integrator?

1. Existing approximations

2. Challenge: show that the integrator is better approximated using full backwater 

3. Show frequency and time test for the three locations. With two kinds of channels.

Tasks to achieve this:

have a bode plot for all loations
check id well, document it



